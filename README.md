# README #

Desenvolvimento do Projeto WebFlix


### Objetivo ###

* Criar uma plataforma de pesquisas de filmes e atores;

### Justificativa ###

* Inserir justificativa do trabalho aqui.


### Info ###

* Versão: v-1.0
* URL: ???
* Data-Base: MySQL 5.7.16(utf8-unicode)
* Linguagem: PHP-7 - JQuery - HTML5 - MySQL


### Contato ###

* Patricia
* inserir seu e-mail aqui
* (51) xxxx -xxxx

### Mantenedores ###

* Repositório mantido por Patricia
* Empresa responsável: WebFlix

### Desenvolvedores & Analistas de Conteúdo Intelectual ###

* Patricia
* colega 2
* colega 3
* colega 4

### Desenvolvedor da Aplicação ###

* Patricia
